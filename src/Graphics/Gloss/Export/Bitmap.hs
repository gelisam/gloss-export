module Graphics.Gloss.Export.Bitmap
    ( exportPictureToBitmap
    , exportPicturesToBitmap
    ) where

import Codec.Picture.Bitmap (writeBitmap)
import qualified Graphics.Gloss.Rendering as Gloss

import Graphics.Gloss.Export.Image

-- | Save a gloss Picture as Bitmap
exportPictureToBitmap :: Size -- ^ width, height in pixels 
                   -> Gloss.Color -- ^ background color
                   -> FilePath -> Gloss.Picture -> IO ()
exportPictureToBitmap  = exportPictureToFormat writeBitmap

-- | Save a gloss animation as Bitmap
exportPicturesToBitmap :: Size        -- ^ width, height in pixels 
                    -> Gloss.Color -- ^ background color
                    -> FilePath
                    -> Animation   -- ^ function that maps from point in time to Picture. analog to Gloss.Animation
                    -> [Float]     -- ^ list of points in time at which to evaluate the animation
                    -> IO ()
exportPicturesToBitmap = exportPicturesToFormat writeBitmap
