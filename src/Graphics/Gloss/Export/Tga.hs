module Graphics.Gloss.Export.Tga
    ( exportPictureToTga
    , exportPicturesToTga
    ) where

import Codec.Picture.Tga (writeTga)
import qualified Graphics.Gloss.Rendering as Gloss

import Graphics.Gloss.Export.Image

-- | Save a gloss Picture as Tga
exportPictureToTga :: Size -- ^ width, height in pixels 
                   -> Gloss.Color -- ^ background color
                   -> FilePath -> Gloss.Picture -> IO ()
exportPictureToTga  = exportPictureToFormat writeTga

-- | Save a gloss animation as Tga
exportPicturesToTga :: Size        -- ^ width, height in pixels 
                    -> Gloss.Color -- ^ background color
                    -> FilePath
                    -> Animation   -- ^ function that maps from point in time to Picture. analog to Gloss.Animation
                    -> [Float]     -- ^ list of points in time at which to evaluate the animation
                    -> IO ()
exportPicturesToTga = exportPicturesToFormat writeTga
