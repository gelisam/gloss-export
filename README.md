# gloss-export

Export gloss pictures as PNG, Bitmap, TGA, TIFF, Gif

Credit to Samuel Gélineau (https://github.com/benl23x5/gloss/pull/23), who provided a working conversion from Picture to PNG.
I just refactored it a little and turned it into a library.